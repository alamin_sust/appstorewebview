package com.example.md_al.appstore;

import android.Manifest;
import android.app.DownloadManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.DownloadListener;
import android.webkit.URLUtil;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_WRITE_EXTERNAL_STORAGE = 1;
    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        webView = (WebView) findViewById(R.id.webview);
        webView.setWebViewClient(new WebViewClient());

        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_EXTERNAL_STORAGE);
        } else {
            //TODO
            webView.setDownloadListener(new DownloadListener() {



                @Override
                public void onDownloadStart(String url, String userAgent,
                                            String contentDisposition, String mimetype,
                                            long contentLength) {
                    DownloadManager.Request request = new DownloadManager.Request(
                            Uri.parse(url));

                    request.allowScanningByMediaScanner();
                    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED); //Notify client once download is completed!
                    DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                    String title = URLUtil.guessFileName(url, contentDisposition, mimetype);
                    request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
                    request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, title);

                    try {
                        dm.enqueue(request);
                    } catch (Exception e) {
                        System.out.println("Here is the bug: LOL<" + e +">LOL");
                    }




                    Toast.makeText(MainActivity.this, "Downloading File", //To notify the Client that the file is being downloaded
                            Toast.LENGTH_SHORT).show();

                }
            });

        }




        webView.loadUrl("http://102.165.51.31:8080/AppStore/home.jsp");

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_WRITE_EXTERNAL_STORAGE:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    //TODO
                    webView.setDownloadListener(new DownloadListener() {



                        @Override
                        public void onDownloadStart(String url, String userAgent,
                                                    String contentDisposition, String mimetype,
                                                    long contentLength) {
                            DownloadManager.Request request = new DownloadManager.Request(
                                    Uri.parse(url));

                            request.allowScanningByMediaScanner();
                            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED); //Notify client once download is completed!
                            DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                            String title = URLUtil.guessFileName(url, contentDisposition, mimetype);
                            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
                            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, title);

                            try {
                                dm.enqueue(request);
                            } catch (Exception e) {
                                System.out.println("Here is the bug: LOL<" + e +">LOL");
                            }




                            Toast.makeText(MainActivity.this, "Downloading File", //To notify the Client that the file is being downloaded
                                    Toast.LENGTH_SHORT).show();

                        }
                    });
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }
}
